from warnings import warn
from os.path import join, dirname
from types import ModuleType
from pytest import fixture

from mypy import api

from budosystems.xtra import mypy_plugin


def test_subproject_import():
    assert isinstance(mypy_plugin, ModuleType)


def test_run_budosystems_models(capsys):
    with capsys.disabled():
        args = [
            '-p', 'budosystems.models',
            '-p', 'budosystems.storage',
            '--config-file', join(dirname(__file__), 'budosystems-core-mypy.ini'),
            '--show-traceback'
        ]

        mypy_out, mypy_err, mypy_exit = api.run(args)

        if mypy_out:
            print('\nType checking report:\n')
            print(mypy_out)

        if mypy_err:
            print('\nError report:\n')
            print(mypy_err)  # stderr

        print('\nExit status:', mypy_exit)

        assert 0 <= mypy_exit < 2, f"mypy did not terminate normally.  Exit status: {mypy_exit}"

        if mypy_exit == 1:
            warn("mypy found some type errors.")
