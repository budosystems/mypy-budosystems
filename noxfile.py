"""Nox configuration."""

import nox

nox.options.sessions = ["tests"]


@nox.session(python=["3.9", "3.10", "3.11", "3.12"])
def tests(session: nox.Session) -> None:
    """Run pytest across multiple python versions."""
    session.install('.[test]')
    session.run('pytest')
